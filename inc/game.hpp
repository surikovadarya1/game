#pragma once
#include <SFML/Graphics.hpp>
#include <sstream>

namespace mt
{
	struct Point
	{
		int x, y;
	};

#define MAX_NUMBER_OF_ITEMS 2
	class Menu
	{
	public:
		Menu();
		~Menu();

		void draw(sf::RenderWindow& window);
		void MoveUp();
		void MoveDown();
		int GetPressedItem() { return selectedItemIndex; }

	private:
		int selectedItemIndex;
		sf::Font font;
		sf::Text menu[MAX_NUMBER_OF_ITEMS];

	};
	bool startGame();
	class Menu1
	{
	public:
		Menu1();
		~Menu1();

		void draw(sf::RenderWindow& window);
		void MoveUp();
		void MoveDown();
		int GetPressedItem() { return selectedItemIndex; }

	private:
		int selectedItemIndex;
		sf::Font font;
		sf::Text menu[MAX_NUMBER_OF_ITEMS];
	};
}