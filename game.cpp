#include <game.hpp>
#include <sstream>
#include <iostream>
namespace mt
{
	Menu::Menu()
	{
		if (!font.loadFromFile("font/arial.ttf"))
		{
			std::cout << "Error";
		}

		menu[0].setFont(font);
		menu[0].setColor(sf::Color::Red);
		menu[0].setString("Play");
		menu[0].setPosition(sf::Vector2f(250, 270));

		menu[1].setFont(font);
		menu[1].setColor(sf::Color::White);
		menu[1].setString("Exit");
		menu[1].setPosition(sf::Vector2f(250, 320));

		selectedItemIndex = 0;

	}
	Menu::~Menu()
	{
	}

	void Menu::draw(sf::RenderWindow& window)
	{
		for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
		{
			window.draw(menu[i]);
		}
	}

	void Menu::MoveUp()
	{
		if (selectedItemIndex - 1 >= 0)
		{
			menu[selectedItemIndex].setColor(sf::Color::White);
			selectedItemIndex--;
			menu[selectedItemIndex].setColor(sf::Color::Red);
		}
	}

	void Menu::MoveDown()
	{
		if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
		{
			menu[selectedItemIndex].setColor(sf::Color::White);
			selectedItemIndex++;
			menu[selectedItemIndex].setColor(sf::Color::Red);
		}
	}

	void death()
	{
		sf::RenderWindow window(sf::VideoMode(600, 600), "Second menu");
		
		sf::Image icon;
		if (!icon.loadFromFile("images/unnamed.jpg"))
		{
			std::cout << "Error";
		}
		window.setIcon(32, 32, icon.getPixelsPtr());
		mt::Menu1 menu1;
		while (window.isOpen())
		{
			sf::Event event;

			while (window.pollEvent(event))
			{
				switch (event.type)
				{
				case sf::Event::KeyReleased:
					switch (event.key.code)
					{
					case sf::Keyboard::Up:
						menu1.MoveUp();
						break;

					case sf::Keyboard::Down:
						menu1.MoveDown();
						break;

					case sf::Keyboard::Return:
						switch (menu1.GetPressedItem())
						{
						case 0:
							window.close();
							mt::startGame();
							break;
						case 1:
							window.close();
							break;
						}

						break;
					}

					break;
				case sf::Event::Closed:
					window.close();

					break;

				}
			}

			window.clear();

			menu1.draw(window);

			window.display();
		}
	}
	bool startGame()
	{

        srand(time(0));
        sf::RenderWindow window(sf::VideoMode(600, 533), "Game");
        window.setFramerateLimit(60);
        sf::Texture t1, t2, t3;
        t1.loadFromFile("images/background.jpg");
        t2.loadFromFile("images/platform.png");
        t3.loadFromFile("images/person.png");
		sf::Image icon;
		if (!icon.loadFromFile("images/unnamed.jpg"))
		{
			std::cout << "Error";
		}
		window.setIcon(32, 32, icon.getPixelsPtr());

        sf::Sprite sBackground(t1), sPlatform(t2), sPerson(t3);

        sPerson.setOrigin(33, 90);
        sPlatform.setOrigin(34, 10);
        mt::Point plat[20];
		sf::Font arial;
		arial.loadFromFile("font/arial.ttf");

		int score = 0;
		std::ostringstream ssScore;
		ssScore << "Score: " << score;
		sf::Text lblScore;
		lblScore.setCharacterSize(30);
		lblScore.setPosition( 10, 10 );
		lblScore.setFont(arial);
		lblScore.setString(ssScore.str());
		lblScore.setColor(sf::Color::Black);


        for (int i = 0; i < 10; i++)
        {
            plat[i].x = rand() % 600;
            plat[i].y = rand() % 533;
        }

        int x = 100, y = 100, h = 200;
        float dx = 0, dy = 0;
        while (window.isOpen())
        {
            sf::Event event;
            while (window.pollEvent(event))
            {
				
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Tab)) { return true; }
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) { return false; }
                if (event.type == sf::Event::Closed)
                    window.close();
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) x += 3;
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) x -= 3;

            dy += 0.2;
            y += dy;
			if (y > 630) { window.close(); death(); }
            sPerson.setPosition(x, y);
            if (y < h)
                for (int i = 0; i < 10; i++)
                {
                    y = h;
                    plat[i].y = plat[i].y - dy;
                    if (plat[i].y > 533) {
                        plat[i].y = 0; plat[i].x = rand() % 600;
                    }
                }
            for (int i = 0; i < 10; i++)
            {
				if ((x + 33 > plat[i].x - 34) && (x - 33 < plat[i].x + 34) && (y + 90 > plat[i].y - 10)
					&& (y + 90 < plat[i].y + 10) && (dy > 0)) {
					dy = -10;
					score++;
					ssScore.str("");
					ssScore << "Score " << score;
					lblScore.setString(ssScore.str());
				}
            }

            window.clear();
            window.draw(sBackground);
            window.draw(sPerson);
			window.draw(lblScore);
            for (int i = 0; i < 10; i++) {
                sPlatform.setPosition(plat[i].x, plat[i].y);
                window.draw(sPlatform);
            }
            window.display();
        }
	}




	Menu1::Menu1()
	{
		if (!font.loadFromFile("font/arial.ttf"))
		{
			std::cout << "Error";
		}
		
		menu[0].setFont(font);
		menu[0].setColor(sf::Color::Red);
		menu[0].setString("Restart");
		menu[0].setPosition(sf::Vector2f(250, 270));

		menu[1].setFont(font);
		menu[1].setColor(sf::Color::White);
		menu[1].setString("Exit");
		menu[1].setPosition(sf::Vector2f(250, 320));

		selectedItemIndex = 0;

	}
	Menu1::~Menu1()
	{
	}

	void Menu1::draw(sf::RenderWindow& window)
	{
		for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
		{
			window.draw(menu[i]);
		}
	}

	void Menu1::MoveUp()
	{
		if (selectedItemIndex - 1 >= 0)
		{
			menu[selectedItemIndex].setColor(sf::Color::White);
			selectedItemIndex--;
			menu[selectedItemIndex].setColor(sf::Color::Red);
		}
	}

	void Menu1::MoveDown()
	{
		if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
		{
			menu[selectedItemIndex].setColor(sf::Color::White);
			selectedItemIndex++;
			menu[selectedItemIndex].setColor(sf::Color::Red);
		}
	}

}